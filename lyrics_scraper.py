


from requests import Session 
from os import listdir,remove 
from json import loads , dumps 
from bs4 import BeautifulSoup 
import concurrent.futures 
from basic_fun import save_file 
from datetime import date 
from bisect import bisect 

def fast_boi(fun,a:list):
  with concurrent.futures.ThreadPoolExecutor() as exe:
    x=exe.map(fun,a)
  return x

session = Session().get 

Bs4_Object = lambda n:BeautifulSoup(session('https://www.lyricscraze.com/category/bollywood-songs/page/%s/'%(n)).text,'lxml')

Bs4_Object_1 = lambda n: BeautifulSoup(session(n).text , 'lxml')

def get_name_and_links(n):
    Soup = Bs4_Object(n)
    al_name_data = Soup.find('div',class_='td-ss-main-content').find_all('h3',class_='entry-title td-module-title')
    name_link = [(x['title'].lower(),x['href']) for x in [x.find('a') for x in al_name_data]]
    return name_link 



def saveing_the_file_json_and_js():
    execute = fast_boi(get_name_and_links,[1,2,100])
    v = ((sorted(sum([i for i in execute],[]))))
    save_file(f'Json/{date.today()}.json',v)
    with open('static/Js/indexs_suj.js','w') as k:
        k.write(f'let suggestions  = { dumps( list(dict(v).keys()),indent=2)}')
    return 1

def open_json_file():
    x = ['Json/'+x for x in  listdir('Json')]
    if not x or len(x)!=1 or x[0]!=f'Json/{date.today()}.json':
        [remove(i) for i in x]
        saveing_the_file_json_and_js()
    with open(f'Json/{date.today()}.json','r') as ff:
        data = loads(ff.read())
    return list(data.keys()),list(data.values())

Name,Link = open_json_file()

def get_image_and_lyrics(n:str):    
    #'td-post-featured-image'
    link = Link[bisect(Name,n.lower())-1]
    Soup_1 = Bs4_Object_1(link)
    print(Soup_1.find('div',class_='td-post-featured-image').find('img')['data-src'])
    print([x.text for x in Soup_1.find_all('p') if '<br>'or 'class' not  in x ][1:-1])

print(get_image_and_lyrics('Baaki Sab First Class Hai Lyrics | Jai Ho 2014'))
