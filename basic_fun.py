
import json

def sort_data(n:dict):
  x=sorted(n.items(),key=lambda t: t[0])
  return dict(x)


def save_file(name,data):
    with open(name,'w') as f:
        f.write(f'{json.dumps(sort_data(dict(data)) , indent=2)}')
    return

